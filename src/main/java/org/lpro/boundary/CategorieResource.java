package org.lpro.boundary;

import java.net.URI;
import java.util.Optional;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.lpro.entity.Categorie;
import org.lpro.entity.Sandwich;

@Stateless
@Path("categories")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CategorieResource {
    
    @Inject
    SandwichManager sm;
    
    @Inject
    CategorieManager cm;
    @Context 
    UriInfo uriInfo;
    
    @GET
    public Response getCategories() {
        JsonObject json = Json.createObjectBuilder()
                .add("type", "collection")
                .add("categories", getCategorieList())
                .build();
        return Response.ok(json).build();
    }
    
    @GET
    @Path("{id}")
    public Response getOneCategorie(@PathParam("id") long id, @Context UriInfo uriInfo) {
        return Optional.ofNullable(cm.findById(id))
                //.map(c -> Response.ok(categorie2Json(c)).build())
                .map(c -> Response.ok(c).build())
                .orElseThrow(() -> new CategorieNotFound("Ressource non disponible "+ uriInfo.getPath()));
    }
    
    @POST
    public Response newCategorie(@Valid Categorie c, @Context UriInfo uriInfo) {
        Categorie newOne = this.cm.save(c);
        long id = newOne.getId();
        URI uri = uriInfo.getAbsolutePathBuilder().path("/" + id).build();
        return Response.created(uri).build();
    }
    
    @DELETE
    @Path("{id}")
    public Response suppression(@PathParam("id") long id) {
        this.cm.delete(id);
        return Response.status(Response.Status.NO_CONTENT).build();
    }
    
    @PUT
    @Path("{id}")
    public Categorie update(@PathParam("id") long id, @Valid Categorie c) {
        c.setId(id);
        return this.cm.save(c);
    }
    
    private JsonObject categorie2Json(Categorie c) {
        JsonObject json = Json.createObjectBuilder()
                .add("type", "resource")
                .add("categorie", buildJson(c))
                .build();
        return json;
    }
    
    private JsonArray getCategorieList() {
        JsonArrayBuilder jab = Json.createArrayBuilder();
        this.cm.findAll().forEach((c) -> {
            jab.add(buildJson(c));
            });
        return jab.build();
    }
    
    private JsonObject buildJson(Categorie c) {
         String uri = uriInfo.getBaseUriBuilder()
            .path(CategorieResource.class)
            .path(c.getId()+"")
            .build()
            .toString();

        String uriSandwich = uriInfo.getBaseUriBuilder()
            .path(CategorieResource.class)
            .path(c.getId() + "")
            .path(SandwichResource.class)
            .build()
            .toString();

        JsonArrayBuilder link = Json.createArrayBuilder();
        link.add(getSelfUri(uri));
        link.add(getSandwichUri(uriSandwich));

        JsonArrayBuilder sandwich = Json.createArrayBuilder();
        c.getSandwich().forEach( s ->
        {
            sandwich.add(buildJsonSandwich(s));
        });
        return Json.createObjectBuilder()
                .add("id",c.getId())
                .add("nom", c.getNom())
                .add("description", c.getDescription())
                .add("sandwichs", sandwich.build())
                .add("link", link)
                .build();
    }    
    
     private JsonValue getSandwichUri(String uriSandwich) 
    {
        return Json.createObjectBuilder()
            .add("href", uriSandwich)
            .add("rel", "sandwichs")
            .build();
	}

    private JsonValue getSelfUri(String uriSelf) {
    return Json.createObjectBuilder()
        .add("href", uriSelf)
        .add("rel", "self")
        .build();
    }
    
    private JsonObject buildJsonSandwich(Sandwich s) {
        String uri = uriInfo.getBaseUriBuilder()
                .path(SandwichResource.class)
                .path(s.getId())                
                .build()
                .toString();
        JsonObject json = Json.createObjectBuilder()
            .add("href", uri)
            .add("rel", "self")
            .build();
        JsonArrayBuilder link = Json.createArrayBuilder();
        link.add(json);
        return Json.createObjectBuilder()
                .add("id",s.getId())
                .add("nom", s.getNom())
                .add("description", s.getDescription())
                .add("typePain", s.getTypePain())
                .add("img", s.getImg())
                .add("link", link)
                .build();
    }
    
    @GET
    @Path("{id}/sandwichs")
    public Response getSandwichCategory(@PathParam("id") long id)
    {
        return Optional.ofNullable(cm.findById(id))
            .map(c -> Response.ok(c).build())
            .orElseThrow(() -> new CategorieNotFound("Ressource non disponible "+ uriInfo.getPath()));
    }

    private JsonObject buildSandwichCategory(Categorie c) 
    {
        JsonArrayBuilder jab = Json.createArrayBuilder();
        c.getSandwich().forEach( s -> 
        {
            jab.add(buildJsonSandwich(s));
        });
        
        return Json.createObjectBuilder()
            .add("sandwichs", jab.build())
            .build();
    }
}
