package org.lpro.boundary;

import java.net.URI;
import java.util.Optional;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.lpro.entity.Sandwich;
import org.lpro.entity.Taille;

@Stateless
@Path("taille")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TailleRessource {
    
    @Inject
    TailleManager tm;
    @Context 
    UriInfo uriInfo;
    
    @GET
    public Response getTaille() {
        JsonObject json = Json.createObjectBuilder()
                .add("type", "collection")
                .add("taille", getTailleList())
                .build();
        return Response.ok(json).build();
    }
    
    @GET
    @Path("{id}")
    public Response getOneTaille(@PathParam("id") long id) {
        return Optional.ofNullable(tm.findById(id))
                //.map(c -> Response.ok(categorie2Json(c)).build())
                .map(c -> Response.ok(c).build())
                .orElseThrow(() -> new CategorieNotFound("Ressource non disponible "+ uriInfo.getPath()));
    }
    
    private Object buildTailleObject(Taille t) 
    {
        return Json.createObjectBuilder()
            .add("taille", buildJson(t))
            .build();
	}
     private JsonObject buildJson(Taille t) {
         String uri = uriInfo.getBaseUriBuilder()
            .path(TailleRessource.class)
            .path(t.getId()+"")
            .build()
            .toString();

        String uriSandwich = uriInfo.getBaseUriBuilder()
            .path(TailleRessource.class)
            .path(t.getId() + "")
            .path(SandwichResource.class)
            .build()
            .toString();

        JsonArrayBuilder link = Json.createArrayBuilder();
        link.add(getSelfUri(uri));
        link.add(getSandwichUri(uriSandwich));

        JsonArrayBuilder sandwich = Json.createArrayBuilder();
        t.getSandwichs().forEach( s ->
        {
            sandwich.add(buildJsonSandwich(s));
        });
        return Json.createObjectBuilder()
                .add("id",t.getId())
                .add("nom", t.getNom())
                .add("prix", t.getPrix())
                .add("sandwich", sandwich.build())
                .add("link", link)
                .build();
    }
      private JsonValue getSandwichUri(String uriSandwich) 
    {
        return Json.createObjectBuilder()
            .add("href", uriSandwich)
            .add("rel", "sandwich")
            .build();
	}

    private JsonValue getSelfUri(String uriSelf) {
    return Json.createObjectBuilder()
        .add("href", uriSelf)
        .add("rel", "self")
        .build();
    }
    
    private JsonObject buildJsonSandwich(Sandwich s) {
        String uri = uriInfo.getBaseUriBuilder()
                .path(SandwichResource.class)
                .path(s.getId())                
                .build()
                .toString();
        JsonObject json = Json.createObjectBuilder()
            .add("href", uri)
            .add("rel", "self")
            .build();
        JsonArrayBuilder link = Json.createArrayBuilder();
        link.add(json);
        return Json.createObjectBuilder()
                .add("id",s.getId())
                .add("nom", s.getNom())
                .add("description", s.getDescription())
                .add("typePain", s.getTypePain())
                .add("img", s.getImg())
                .add("link", link)
                .build();
    }    
    
    @POST
    public Response newTaille(@Valid Taille t, @Context UriInfo uriInfo) {
        Taille newOne = this.tm.save(t);
        long id = newOne.getId();
        URI uri = uriInfo.getAbsolutePathBuilder().path("/" + id).build();
        return Response.created(uri).build();
    }
    
    @DELETE
    @Path("{id}")
    public Response suppression(@PathParam("id") long id) {
        this.tm.delete(id);
        return Response.status(Response.Status.NO_CONTENT).build();
    }
    
    @PUT
    @Path("{id}")
    public Taille update(@PathParam("id") long id, @Valid Taille t) {
        t.setId(id);
        return this.tm.save(t);
    }
    
    private JsonObject taille2Json(Taille t) {
        JsonObject json = Json.createObjectBuilder()
                .add("type", "resource")
                .add("Taille", buildJson(t))
                .build();
        return json;
    }
    
    private JsonArray getTailleList() {
        JsonArrayBuilder jab = Json.createArrayBuilder();
        this.tm.findAll().forEach((t) -> {
            jab.add(buildJson(t));
            });
        return jab.build();
    }       
    
    @GET
    @Path("{id}/sandwichs")
    public Response getSandwichTaille(@PathParam("id") long id)
    {
        return Optional.ofNullable(tm.findById(id))
            .map(c -> Response.ok(c).build())
            .orElseThrow(() -> new TailleNotFound("Ressource non disponible "+ uriInfo.getPath()));
    }

    private JsonObject buildSandwichTaille(Taille t) 
    {
        JsonArrayBuilder jab = Json.createArrayBuilder();
        t.getSandwichs().forEach( s -> 
        {
            jab.add(buildJsonSandwich(s));
        });
        
        return Json.createObjectBuilder()
            .add("sandwichs", jab.build())
            .build();
    }
}
