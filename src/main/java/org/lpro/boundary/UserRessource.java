/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lpro.boundary;
import org.lpro.entity.User;
import org.mindrot.jbcrypt.BCrypt;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.List;
import org.lpro.control.KeyManagement;


@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserRessource {

    @EJB
    UserManager userManager;

    KeyManagement kg;

    @GET
    public Response getAllUsers(@Context UriInfo uriInfo){
        List<User> list_user = this.userManager.findAll();
        GenericEntity<List<User>> list = new GenericEntity<List<User>>(list_user) {
        };
        return Response.ok(list, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/{id_user}")
    public Response getUser(@PathParam("id_user") String id, @Context UriInfo uriInfo){
        User user = this.userManager.findById(id);
        if(user != null){
            return Response.ok(user).build();
        }else{
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response addUser(@FormParam("login") String login, @FormParam("password") String password, @Context UriInfo uriInfo){
        String passHashed = BCrypt.hashpw(password, BCrypt.gensalt());
        User user = new User(login, passHashed);
        User newUser = this.userManager.save(user);
        URI uri = uriInfo.getAbsolutePathBuilder().path(newUser.getId()).build();
        return Response.created(uri)
                .entity(newUser)
                .build();
    }

    @DELETE
    @Path("/{id_user}")
    public void deleteUser(@PathParam("id_user") String id){
        this.userManager.delete(id);
    }

}
