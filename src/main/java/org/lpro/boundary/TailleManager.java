package org.lpro.boundary;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.CacheStoreMode;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.lpro.entity.Categorie;
import org.lpro.entity.Taille;

@Stateless
public class TailleManager {
    
    @PersistenceContext 
    EntityManager em;
    
    public Taille findById(long id) {
        return this.em.find(Taille.class, id);
    }
    
    public List<Taille> findAll() {
        Query q = this.em.createNamedQuery("Taille.findAll", Taille.class);
        q.setHint("javax.persistence.cache.storeMode", CacheStoreMode.REFRESH);
        return q.getResultList();
    }
    
    public Taille save(Taille t) {
        return this.em.merge(t);
    }
    
    public void delete(long id) {
        try {
            Taille ref = this.em.getReference(Taille.class, id);
            this.em.remove(ref);
        } catch (EntityNotFoundException enfe) {
            // rien à faire
        } 
    }
}
