/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lpro.boundary;

import java.net.URI;
import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.lpro.entity.Categorie;
import org.lpro.entity.Commande;
import org.lpro.entity.Sandwich;
import org.lpro.entity.Taille;


@Stateless
@Path("sandwichs")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SandwichResource {
    
    @Inject
    SandwichManager sm;
    UriInfo uriInfo;
    
    @GET
    public Response getSandwichs() {
        JsonObject json = Json.createObjectBuilder()
                .add("type", "sandwich")
                .add("categories", getSandwichList())
                .build();
        return Response.ok(json).build();
    }
    @GET
    public Response getSandwichs(
            @QueryParam("t") String type,
            @QueryParam("img") int img,
            @DefaultValue("1") @QueryParam("page") int page,
            @DefaultValue("10") @QueryParam("size") int size) {
        JsonObject json = Json.createObjectBuilder()
                .add("type", "sandwich")
                .add("sandwichs", getSandwichList(type, img, page, size))
                .build();
        return Response.ok(json).build();
    }
    
    @GET
    @Path("{id}")
    public Response getOneSandwich(@PathParam("id") String id, @Context UriInfo uriInfo) {
        return Optional.ofNullable(sm.findById(id))
                .map(s -> Response.ok(s).build())
                .orElseThrow(() -> new SandwichNotFound("Ressource non disponible "+ uriInfo.getPath()));
    }
    
    @POST
    public Response newSandwich(@Valid Sandwich s, @Context UriInfo uriInfo) {
        Sandwich newOne = this.sm.save(s);
        String id = newOne.getId();
        URI uri = uriInfo.getAbsolutePathBuilder().path("/" + id).build();
        return Response.created(uri).build();
    }
    
    @DELETE
    @Path("{id}")
    public Response suppression(@PathParam("id") long id) {
        this.sm.delete(id);
        return Response.status(Response.Status.NO_CONTENT).build();
    }
    
    @PUT
    @Path("{id}")
    public Sandwich update(@PathParam("id") String id, @Valid Sandwich s) {
        s.setId(id);
        return this.sm.save(s);
    }
    
    private JsonArray getSandwichList() {
        JsonArrayBuilder jab = Json.createArrayBuilder();
        this.sm.findAll().forEach((s) -> {
            jab.add(buildJson(s));
            });
        return jab.build();
    }
    
    private JsonValue getSandwichList(String type, int img, int page, int size) {
        JsonArrayBuilder jab = Json.createArrayBuilder();
        this.sm.find(type, img, page, size).forEach((s) -> {
            jab.add(buildJson(s));
            });
        return jab.build();
    }
    
    private JsonObject buildJson(Sandwich s) {
        JsonObjectBuilder ret = Json.createObjectBuilder()
                .add("id",s.getId())
                .add("nom", s.getNom())
                .add("desc", s.getDescription())
                .add("typePain", s.getTypePain())
                .add("link", buildJsonLink(s.getId()));
        if (s.getImg() != null) {
            ret.add("img", s.getImg());
        }
        return ret.build();
    }
    private JsonValue buildJsonLink(String id) 
    {
        return Json.createObjectBuilder()
                .add("self", Json.createObjectBuilder()
                            .add("href", "/sandwichs/" + id + "/")
                            .build())
                .build();

    }
    
    //Catégories des sandwichs
    @GET
    @Path("{id}/categories")
    public Response getCategoriesSandwich(@PathParam("id") String id)
    {
        return Optional.ofNullable(sm.findById(id))
            .map(s -> Response.ok(buildCategorySandwich(s)).build())
            .orElseThrow( () -> new SandwichNotFound("Ressource non disponible " + uriInfo.getPath()));
    }

    private JsonObject buildCategorySandwich(Sandwich s)
    {
        JsonArrayBuilder jab = Json.createArrayBuilder();
        s.getCategorie().forEach( c ->
        {
            jab.add(buildJsonCategory(c));
        });

        return Json.createObjectBuilder()
            .add("categories", jab.build())
            .build();
    }

    private JsonValue buildJsonCategory(Categorie c) 
    {
        String uriCategory = uriInfo.getBaseUriBuilder()
            .path(CategorieResource.class)
            .path(c.getId() + "")
            .build()
            .toString();

        JsonObject job = Json.createObjectBuilder()
            .add("href", uriCategory)
            .add("rel", "self")
            .build();
        
        JsonArrayBuilder link = Json.createArrayBuilder();
        link.add(job);

        return Json.createObjectBuilder()
            .add("id", c.getId())
            .add("nom", c.getNom())
            .add("description", c.getDescription())
            .add("link", link)
            .build();
	}
    //sandwich avec les tailles
     @GET
    @Path("{id}/tailles")
    public Response getTailleSandwich(@PathParam("id") String id)
    {
        return Optional.ofNullable(sm.findById(id))
            .map(s -> Response.ok(buildCategorySandwich(s)).build())
            .orElseThrow( () -> new SandwichNotFound("Ressource non disponible " + uriInfo.getPath()));
    }

    private JsonObject buildTailleSandwich(Sandwich s)
    {
        JsonArrayBuilder jab = Json.createArrayBuilder();
        s.getTaille().forEach( t ->
        {
            jab.add(buildJsonTaille(t));
        });

        return Json.createObjectBuilder()
            .add("taille", jab.build())
            .build();
    }

    private JsonValue buildJsonTaille(Taille t) 
    {
        String uriTaille = uriInfo.getBaseUriBuilder()
            .path(TailleRessource.class)
            .path(t.getId() + "")
            .build()
            .toString();

        JsonObject job = Json.createObjectBuilder()
            .add("href", uriTaille)
            .add("rel", "self")
            .build();
        
        JsonArrayBuilder link = Json.createArrayBuilder();
        link.add(job);

        return Json.createObjectBuilder()
            .add("id", t.getId())
            .add("nom", t.getNom())
            .add("prix", t.getPrix())
            .add("link", link)
            .build();
	}
    
    
    
}
