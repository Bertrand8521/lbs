/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lpro.boundary;

import javax.ejb.Stateless;
import javax.persistence.CacheStoreMode;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.lpro.entity.Carte;

@Stateless
@Transactional
public class CarteManager {
        @PersistenceContext
    EntityManager em;

    public Carte findById(String uid)
    {
        return this.em.find(Carte.class, uid);
    }

    public Carte save(Carte c)
    {
        return this.em.merge(c);
    }
}
