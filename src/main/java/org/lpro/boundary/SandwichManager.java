/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lpro.boundary;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.CacheStoreMode;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.lpro.entity.Sandwich;

@Stateless
public class SandwichManager {
    
    @PersistenceContext 
    EntityManager em;
    
    public Sandwich findById(String uid) {
        return this.em.find(Sandwich.class, uid);
    }
    
    public List<Sandwich> findAll() {
        Query q = this.em.createNamedQuery("Sandwich.findAll", Sandwich.class);
        q.setHint("javax.persistence.cache.storeMode", CacheStoreMode.REFRESH);
        return q.getResultList();
    }
    
    public List<Sandwich> find(String type, int img, int page, int size) {
        String queryString = "SELECT s FROM Sandwich s WHERE 1=1";
        if (type != null) {
            queryString += " AND s.typePain = :type";
        }
        if (img != 0) {
            queryString += " AND s.img IS NOT NULL";
        }
        Query query = em.createQuery(queryString);
        if (type != null) {
            query.setParameter("type", type);
        }
        query.setFirstResult((page-1) * size);
        query.setMaxResults(size); 
        List<Sandwich> sandwichs = query.getResultList();
        return sandwichs;
    }
    
    public Sandwich save(Sandwich s) {
        return this.em.merge(s);
    }
    
    public void delete(long id) {
        try {
            Sandwich ref = this.em.getReference(Sandwich.class, id);
            this.em.remove(ref);
        } catch (EntityNotFoundException enfe) {
            // rien à faire
        } 
    }   
    
    public long count()
    {
        Query number = em.createQuery("SELECT count(s.id) from Sandwich s");
        
        return (long) number.getSingleResult();
    }


}
