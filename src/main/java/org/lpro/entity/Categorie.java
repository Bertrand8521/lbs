package org.lpro.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@NamedQuery(name="Categorie.findAll",query="SELECT c FROM Categorie c")
public class Categorie implements Serializable{

    @Id
    @GeneratedValue
    private long id;
    @NotNull
    private String nom;
    @NotNull
    private String description;
    @ManyToMany
    @JoinTable(name="cat_sand")
    private List<Sandwich> sandwich = new ArrayList<Sandwich>();

    public Categorie() {
    }

    public Categorie(long id, String nom, String description) {
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.sandwich = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public List<Sandwich> getSandwich() 
    {
        return sandwich;
    }

    public void setSandwich(List<Sandwich> sandwich) 
    {
        this.sandwich = sandwich;
    }
    
    
}
