/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lpro.entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Carte implements Serializable {

    @Id
    private Long id;
    private String uid;
    private float prix;
    private float prix_reduit;
    @OneToMany(mappedBy="carte")
    private List<Commande> commande;
    
    
    public Carte() {
    }
    
    public Carte(String uid, float prix, float prix_reduit) 
    {
        this.uid = uid;
        this.prix = 0;
        this.prix_reduit = 0;

        this.commande = new ArrayList<Commande>();
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    } 

    public String getUid() 
    {
        return uid;
    }

    public void setUid(String uid)
     {
        this.uid = uid;
    }  

    public float getPrix() 
    {
        return prix;
    }

    public void setPrix(float prix) 
    {
        this.prix = prix;
    }

    public float getPrixReduit() 
    {
        return prix_reduit;
    }

    public void setPrixReduit(float prix_reduit) 
    {
        this.prix_reduit = prix_reduit;
    }

    public List<Commande> getCommande() 
    {
        return commande;
    }

    public void setCommande(List<Commande> commande) 
    {
        this.commande = commande;
    }
    
}
