package org.lpro.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class Commande implements Serializable {
    
    @Id
    private String id;
    @NotNull
    private String nom;
    private String mail;
    private String dateLivraison;
    private String heureLivraison;
    private String token;
    private boolean commande_paye;
    
    @ManyToOne
    private Carte carte;
    @ManyToMany
    private List<Sandwich> sandwich = new ArrayList<>();

    public Commande() {
    }

    public Commande(String nom, String mail, String dateLivraison, String heureLivraison) {
        this.nom = nom;
        this.mail = mail;
        this.dateLivraison = dateLivraison;
        this.heureLivraison = heureLivraison;
        this.commande_paye=false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getDateLivraison() {
        return dateLivraison;
    }

    public void setDateLivraison(String dateLivraison) {
        this.dateLivraison = dateLivraison;
    }

    public String getHeureLivraison() {
        return heureLivraison;
    }

    public void setHeureLivraison(String heureLivraison) {
        this.heureLivraison = heureLivraison;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
    
    public Carte getCarte() 
    {
        return carte;
    }
    
     public void setCarte(Carte carte) 
    {
        this.carte = carte;
    }
    
     public boolean commande_paye() 
    {
        return commande_paye;
    }

    public void setPaye(boolean commande_paye) 
    {
        this.commande_paye = commande_paye;
    }
}


