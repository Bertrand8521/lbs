/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lpro.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@NamedQuery(name="Sandwich.findAll",query="SELECT s FROM Sandwich s")
public class Sandwich implements Serializable {
    @Id
//    @GeneratedValue TODO pas de auto-increment, gérer manuellement
    private String id;
    @NotNull
    private String nom;
    @NotNull
    private String description;
    @NotNull
    @Column(name="type_pain")
    private String typePain;
    private String img;
    @ManyToMany(mappedBy="sandwich")
    private List<Categorie> categorie = new ArrayList<Categorie>();
    private List<Taille> taille = new ArrayList<Taille>();
    

    public Sandwich() {
        
    }
    
    public Sandwich(String id, String nom, String description, String typePain, String img) {
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.typePain = typePain;
        this.img = img;
        this.categorie = new ArrayList<>();
        this.taille = new ArrayList<>();
    }
    
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the typePain
     */
    public String getTypePain() {
        return typePain;
    }

    /**
     * @param typePain the typePain to set
     */
    public void setTypePain(String typePain) {
        this.typePain = typePain;
    }

    /**
     * @return the img
     */
    public String getImg() {
        return img;
    }

    /**
     * @param img the img to set
     */
    public void setImg(String img) {
        this.img = img;
    }
    
    public List<Taille> getTaille() 
    {
        return taille;
    }

    public void setTailles(List<Taille> taille) 
    {
        this.taille = taille;
    }
    
    public List<Categorie> getCategorie() 
    {
        return categorie;
    }

    public void setCategorie(List<Categorie> categorie) 
    {
        this.categorie = categorie;
    }
    
    
    
}
