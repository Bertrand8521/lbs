/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lpro.entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@NamedQuery(name="Taille.findAll",query="SELECT t FROM Taille t")
public class Taille implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    @NotNull
    private String nom;
    @NotNull
    private int prix;
    @ManyToMany
    private List<Sandwich> sandwich = new ArrayList<Sandwich>();
    
    public Taille() {
        
    }
    
    public Taille(long id, String nom, int prix)
    {
        this.id = id;
        this.nom = nom;
        this.prix = prix;
        this.sandwich = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getNom() 
    {
        return nom;
    }
    
    public void setNom(String nom) 
    {
        this.nom = nom;
    }
    
    public float getPrix() 
    {
        return prix;
    }
    
    public void setPrix(int prix) 
    {
        this.prix = prix;
    }

   public List<Sandwich> getSandwichs() 
    {
        return sandwich;
    }
    
    
}
